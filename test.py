import subprocess
import curses
from curses import wrapper


class GitInteractive():

    def __init__(self, stdscr):
        self.stdscr = stdscr
        curses.curs_set(0)
        self.init_branches()

    def init_branches(self):

        self.branches = []
        self.selected_branch_index = 0

        branch_output = subprocess.run(['git', 'branch'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        branch_strs = branch_output.stdout.decode().strip().split('\n')

        for index, branch_str in enumerate(branch_strs):

            branch_str = branch_str.strip()

            if branch_str[0] == '*':
                self.selected_branch_index = index
                branch_str = branch_str[1:].strip()

            self.branches.append(branch_str)

    def run(self):

        while True:

            self.stdscr.clear()
            self.stdscr.addstr(self.get_option_ui())

            key = self.stdscr.getch()

            if key == 106: # j
                self.move_down()
            elif key == 107: # k
                self.move_up()
            elif key == 10: # Enter
                self.switch_to_branch()
                break
            elif key == 113: # q
                break

    def get_option_ui(self):
        return '\n'.join([
            ('*' if index == self.selected_branch_index else ' ')
            + ' ' + branch
            for index, branch in enumerate(self.branches)
        ])

    def move_down(self):

        self.selected_branch_index += 1

        if self.selected_branch_index >= len(self.branches):
            self.selected_branch_index = 0

    def move_up(self):

        self.selected_branch_index -= 1

        if self.selected_branch_index < 0:
            self.selected_branch_index = len(self.branches) - 1

    def switch_to_branch(self):
        subprocess.run(['git', 'checkout', self.branches[self.selected_branch_index]])


def main(stdscr):
    git_interactive = GitInteractive(stdscr)
    git_interactive.run()


wrapper(main)
